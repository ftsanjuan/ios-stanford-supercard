//
//  SuperCardAppDelegate.h
//  Supercard
//
//  Created by Francis San Juan on 2013-08-06.
//  Copyright (c) 2013 Francis San Juan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SuperCardAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
