//
//  main.m
//  Supercard
//
//  Created by Francis San Juan on 2013-08-06.
//  Copyright (c) 2013 Francis San Juan. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SuperCardAppDelegate.h"

int main(int argc, char *argv[])
{
  @autoreleasepool {
      return UIApplicationMain(argc, argv, nil, NSStringFromClass([SuperCardAppDelegate class]));
  }
}
