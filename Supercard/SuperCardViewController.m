//
//  SuperCardViewController.m
//  Supercard
//
//  Created by Francis San Juan on 2013-08-06.
//  Copyright (c) 2013 Francis San Juan. All rights reserved.
//

#import "SuperCardViewController.h"
#import "PlayingCardView.h"
#import "PlayingCardDeck.h"
#import "PlayingCard.h"

@interface SuperCardViewController ()
@property (weak, nonatomic) IBOutlet PlayingCardView *playingCardView;
@property (strong, nonatomic) Deck *deck;

@end

@implementation SuperCardViewController

-(Deck *)deck
{
  if (!_deck) _deck = [[PlayingCardDeck alloc] init];
  return _deck;
}

-(void)setPlayingCardView:(PlayingCardView *)playingCardView
{
  _playingCardView = playingCardView;
  [self drawRandomPlayingCard];

  // add gesture recognizer for pinch
  [playingCardView addGestureRecognizer:[[UIPinchGestureRecognizer alloc] initWithTarget:self.playingCardView action:@selector(pinch:)]];
}

- (void)drawRandomPlayingCard
{
  Card *card = [self.deck drawRandomCard];
  if ([card isKindOfClass:[PlayingCard class]]) {
    PlayingCard *playingCard = (PlayingCard *)card;
    self.playingCardView.rank = playingCard.rank;
    self.playingCardView.suit = playingCard.suit;
  }
}

- (IBAction)swipe:(UISwipeGestureRecognizer *)sender {
  // add a card flip animation
  [UIView transitionWithView:self.playingCardView
                     duration:0.5
                      options:UIViewAnimationOptionTransitionFlipFromLeft
                   animations:^{
                     // code required to occur between transition
                     // goes here, i.e. get a new playing card
                     // or change the card face here
                     if (!self.playingCardView.faceUp) [self drawRandomPlayingCard];
                     self.playingCardView.faceUp = !self.playingCardView.faceUp;
                   }
                   completion:NULL];
}

@end
