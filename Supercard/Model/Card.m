//
//  Card.m
//  Matchismo
//
//  Created by Francis San Juan on 2013-07-16.
//  Copyright (c) 2013 Francis San Juan. All rights reserved.
//

#import "Card.h"

@implementation Card

- (int)match:(NSArray *)otherCards {
  int score = 0;

  for (Card* card in otherCards) {
    if ([card.contents isEqualToString:self.contents]) {
      score = 1;
    }
  }

  return score;
}

@end
