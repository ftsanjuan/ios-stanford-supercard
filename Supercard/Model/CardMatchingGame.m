//
//  CardMatchingGame.m
//  Matchismo
//
//  Created by Francis San Juan on 2013-07-16.
//  Copyright (c) 2013 Francis San Juan. All rights reserved.
//

#import "CardMatchingGame.h"

#define MATCH_BONUS 4
#define MISMATCH_PENALTY 2
#define FLIP_COST 1

@interface CardMatchingGame()

// this 'score' property is read-only from PUBLIC API perspective
// helpful for other coders to see this being explicitly stated in
// the PRIVATE implementation that this is readwrite (default for a property),
// because of discrepancy between public and private
@property (readwrite, nonatomic) int score;
@property (strong, nonatomic) NSMutableArray *cards; // of Card
@end

@implementation CardMatchingGame

- (NSMutableArray *)cards
{
  if (!_cards) _cards = [[NSMutableArray alloc] init];
  return _cards;
}

- (void)flipCardAtIndex:(NSUInteger)index
{
  Card *card = [self cardAtIndex:index];

  if (card && !card.isUnplayable) {
    // ensure card is facedown before doing any checking
    if (!card.isFaceUp) {
      // handle case that card is flipped up and matches another card flipped up
      for (Card *otherCard in self.cards) {
        // found another card that is faced up & playable
        if (otherCard.isFaceUp && !otherCard.isUnplayable) {

          // note that the match: method requires an array param
          int matchScore = [card match:@[otherCard]];

          // IF CARDS MATCH
          if (matchScore) {
            // turn cards face down and assign points
            card.unplayable = YES;
            otherCard.unplayable = YES;

            // game will assign positive points if cards match
            // but will deduct points if it doesn't match
            self.score += matchScore * MATCH_BONUS;
          }
          // CARDS MISMATCH
          else {
            otherCard.faceUp = NO;
            self.score -= MISMATCH_PENALTY;
          }
          break;
        }
      }
      // prevent flipping too many cards
      self.score -= FLIP_COST;
    }
    // flip the card
    card.faceUp = !card.isFaceUp;
  }
}

// overridden! must use initWithCardCount: usingDeck:
- (id)init
{
  NSLog(@"Error: call initWithCardCount: usingDeck: designated initializer to initialize");
  return nil;
}

- (id)initWithCardCount:(NSUInteger)count usingDeck:(Deck *)deck
{
  self = [super init];

  if (self) {
    for (int i = 0; i < count; i++) {
      Card *card = [deck drawRandomCard];

      // handle the case that a game initialized with a card count
      // that is less than the number of cards in the specified deck
      // return nil if we're unable to initialize
      if (card) {
        self.cards[i] = card;
      } else {
        self = nil;
        break;
      }
    }
  }

  return self;
}

- (Card *)cardAtIndex:(NSUInteger)index
{
  return (index < [self.cards count]) ? self.cards[index] : nil;
}

@end
